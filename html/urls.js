var src           = "http://solgenomics.net/jbrowse/current/?data=data%2Fjson%2FSL2.50";
var gff_file_type = "JBrowse/Store/SeqFeature/GFF3";
var glyph_canvas  = "JBrowse/View/Track/CanvasFeatures";
var glyph_align   = "JBrowse/View/Track/Alignments2";
//var glyph_canvas  = "JBrowse/View/Track/Alignments2";
var dropbox_url   = "https://dl.dropboxusercontent.com/u/753166/jbrowse/";
var ext           = '.gff3'

var color1 = "red";
var color2 = "green";
var color3 = "blue";
var color4 = "purple";
var color5 = "black";

var data = [
//            name                                     file_type      glyph_type    color  file_url

//["gbrowse repeats All"                    , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats.gff3"                         ],
["gbrowse repeats Copia"                  , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats.gff3.Copia.gff3"              ],
["gbrowse repeats Gypsy"                  , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats.gff3.Gypsy.gff3"              ],
["gbrowse repeats others"                 , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats.gff3.others.gff3"             ],
//["gbrowse repeats All PGSC"               , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats.gff3.PGSC.gff3"               ],
["gbrowse repeats All scaffold"           , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats.gff3.scaffold.gff3"           ],
//["gbrowse repeats SINE"                   , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats.gff3.SINE.gff3"               ],

//aggressive QUAL < 200
//["gbrowse repeats aggressive All"         , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats_aggressive.gff3"              ],
["gbrowse repeats aggressive Copia"       , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats_aggressive.gff3.Copia.gff3"   ],
["gbrowse repeats aggressive Gypsy"       , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats_aggressive.gff3.Gypsy.gff3"   ],
["gbrowse repeats aggressive others"      , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats_aggressive.gff3.others.gff3"  ],
//["gbrowse repeats aggressive All PGSC"    , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats_aggressive.gff3.PGSC.gff3"    ],
["gbrowse repeats aggressive All scaffold", gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats_aggressive.gff3.scaffold.gff3"],
//["gbrowse repeats aggressive SINE"        , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_repeats_aggressive.gff3.SINE.gff3"    ],
    
//["gbrowse BAC All"                        , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_SL_BAC.gff3"                          ],
//["gbrowse BAC clone"                      , gff_file_type, glyph_align , color1, dropbox_url+"gbrowse_SL_BAC.gff3.BAC_clone.gff3"           ],
//["gbrowse BAC end"                        , gff_file_type, glyph_align , color1, dropbox_url+"gbrowse_SL_BAC.gff3.BAC_end.gff3"             ],

//["gbrowse BAC problematic All"            , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_SL_BAC_f.gff3"                        ],
//["gbrowse BAC clone problematic "         , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_SL_BAC_f.gff3.BAC_clone.gff3"         ],
//["gbrowse BAC end problematic"            , gff_file_type, glyph_canvas, color1, dropbox_url+"gbrowse_SL_BAC_f.gff3.BAC_end.gff3"           ],
    
//["assembly"                               , gff_file_type, glyph_canvas, color1, dropbox_url+"ITAG2.4_assembly.gff3"                        ],
  ["assembly contigs"                       , gff_file_type, glyph_align , color2, dropbox_url+"ITAG2.4_assembly.gff3.contig.gff3"            ],
//["assembly gaps"                          , gff_file_type, glyph_align , color2, dropbox_url+"ITAG2.4_assembly.gff3.remark.gff3"            ],
  ["assembly scaffolds"                     , gff_file_type, glyph_align , color2, dropbox_url+"ITAG2.4_assembly.gff3.supercontig.gff3"       ],
    
//["BAC All"                                , gff_file_type, glyph_canvas, color3, dropbox_url+"ITAG2.4_genomic_reagents.gff3"                ],
  ["BAC clone"                              , gff_file_type, glyph_align , color3, dropbox_url+"ITAG2.4_genomic_reagents.gff3.BAC_clone.gff3" ],
//["BAC end"                                , gff_file_type, glyph_align , color3, dropbox_url+"ITAG2.4_genomic_reagents.gff3.BAC_end.gff3"   ],

//["repeats All"                            , gff_file_type, glyph_canvas, color4, dropbox_url+"ITAG2.4_repeats.gff3"                         ],
//["repeats Copia"                          , gff_file_type, glyph_align , color4, dropbox_url+"ITAG2.4_repeats.gff3.Copia.gff3"              ],
//["repeats Gypsy"                          , gff_file_type, glyph_align , color4, dropbox_url+"ITAG2.4_repeats.gff3.Gypsy.gff3"              ],
//["repeats others"                         , gff_file_type, glyph_align , color4, dropbox_url+"ITAG2.4_repeats.gff3.others.gff3"             ],
//["repeats PGSC"                           , gff_file_type, glyph_canvas, color4, dropbox_url+"ITAG2.4_repeats.gff3.PGSC.gff3"               ],
//["repeats scaffold"                       , gff_file_type, glyph_canvas, color4, dropbox_url+"ITAG2.4_repeats.gff3.scaffold.gff3"           ],
//["repeats SINE"                           , gff_file_type, glyph_align , color4, dropbox_url+"ITAG2.4_repeats.gff3.SINE.gff3"               ],

  ["OM all"                                 , gff_file_type, glyph_canvas, color1, dropbox_url+"S_lycopersicum_chromosomes.2.50.BspQI_to_EXP_REFINEFINAL1_xmap.txt.augmented.tsv.gff3"],
  ["OM conf10"                              , gff_file_type, glyph_canvas, color1, dropbox_url+"S_lycopersicum_chromosomes.2.50.BspQI_to_EXP_REFINEFINAL1_xmap.txt.augmented.tsv_Confidence_ge_10.report.tsv.gff3"],
  ["OM conf10 broken"                       , gff_file_type, glyph_canvas, color1, dropbox_url+"S_lycopersicum_chromosomes.2.50.BspQI_to_EXP_REFINEFINAL1_xmap.txt.augmented.tsv_Confidence_ge_10__meta_num_orientations_gt_1__meta_is_max_confidence_for_qry_chrom_eq_T.report.tsv.gff3"],

  ["Ns"                                     , gff_file_type, glyph_align , color5, dropbox_url+"S_lycopersicum_chromosomes.2.50.fa.gff3"      ],

//["agp All"                                , gff_file_type, glyph_canvas, color1, dropbox_url+"SL2.50ch_from_sc.agp.gff3"                    ],
//["agp contig"                             , gff_file_type, glyph_canvas, color1, dropbox_url+"SL2.50ch_from_sc.agp.gff3.contig.gff3"        ],
  ["agp gap"                                , gff_file_type, glyph_align , color2, dropbox_url+"SL2.50ch_from_sc.agp.gff3.gap.gff3"           ],
//["agp scaffold"                           , gff_file_type, glyph_canvas, color1, dropbox_url+"SL2.50ch_from_sc.agp.gff3.scaffold.gff3"      ],
];




//# functions.conf
//customColor = function(feature) {
//    return feature.get("type")=="mRNA" ? "green" : "blue";
//  }
//You can then include the file in your trackList.json by adding "include": "functions.conf", and then the functions from this file can be referenced in your trackList.json using "variable interpolation".
//"style": {
//   "color":"{customColor}"
//}

var stores  = {};
var tracks  = [];
for ( var dpos in data ) {
    var datal          = data[dpos];
    var name           = datal[0];
    var file_type      = datal[1];
    var glyph_type     = datal[2];
    var color          = datal[3];
    var file_url       = datal[4];
    var filenum        = parseInt(dpos)+1;
    var file_url_small = dropbox_url+'small/'+filenum+ext;
    
    console.log("name: "+name+" file_type: "+file_type+" glyph_type: "+glyph_type+" color: "+color+" file_url_small: "+file_url_small+" file_url: "+file_url);

    var s_name     = "s" + dpos;
    stores[s_name] = { "type": file_type, "urlTemplate": file_url_small };
    tracks.push({
            "label": name,
            "store": s_name,
            "type" : glyph_type,
            "style": {
                "color": color
            }
    });
}
