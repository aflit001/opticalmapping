set -xeu

infile=data/S_lycopersicum_chromosomes.2.50.BspQI_to_EXP_REFINEFINAL1_xmap.txt
augmented=$infile.augmented.tsv
filtered=${augmented}_Confidence_ge_10__meta_num_orientations_gt_1__meta_is_max_confidence_for_qry_chrom_eq_T.report.tsv
delta=$filtered.delta
gff=$filtered.gff3

cols_to_escape=info/gff_cols_to_escape.txt
chromosome_names=info/S_lycopersicum_chromosomes.2.50.chromosome_names.txt

rm $augmented || true
if [[ ! -f "$augmented" ]]; then
    ./om_augmenter.py $infile -g -c
fi


rm $filtered || true
if [[ ! -f "$filtered" ]]; then
    ./om_filter.py $augmented --filter Confidence:ge:10 --filter _meta_num_orientations:gt:1 --filter _meta_is_max_confidence_for_qry_chrom:eq:T
fi

#exit 0
#rm $delta || true
if [[ ! -f "$delta" ]]; then
    ./om_to_delta.py $filtered
fi


#rm $gff || true
if [[ ! -f "$gff" ]]; then
    ./om_to_gff.py --names-from-file $chromosome_names --exclude-cols-from-file $cols_to_escape $filtered
fi


./om_filter.py    $augmented --filter Confidence:ge:10
./om_filter.py    $augmented --filter Confidence:ge:10 --filter _meta_num_qry_matches:gt:1

./om_to_gff.py   ${augmented}                             --names-from-file $chromosome_names --exclude-cols-from-file $cols_to_escape
./om_to_gff.py   ${augmented}_Confidence_ge_10.report.tsv --names-from-file $chromosome_names --exclude-cols-from-file $cols_to_escape

./om_to_delta.py ${augmented}_Confidence_ge_10__meta_num_qry_matches_gt_1.report.tsv
